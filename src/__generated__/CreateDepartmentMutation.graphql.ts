/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type CreateDepartmentInput = {
    clientMutationId?: string | null;
    department?: DepartmentInput | null;
    parent?: string | null;
};
export type DepartmentInput = {
    description?: string | null;
    id?: string | null;
    metadata?: string | null;
    name?: string | null;
};
export type CreateDepartmentMutationVariables = {
    input?: CreateDepartmentInput | null;
};
export type CreateDepartmentMutationResponse = {
    readonly createDepartment: {
        readonly clientMutationId: string;
        readonly payload: {
            readonly description: string;
            readonly id: string;
            readonly metadata: string | null;
            readonly name: string;
        };
    };
};
export type CreateDepartmentMutation = {
    readonly response: CreateDepartmentMutationResponse;
    readonly variables: CreateDepartmentMutationVariables;
};



/*
mutation CreateDepartmentMutation(
  $input: CreateDepartmentInput
) {
  createDepartment(input: $input) {
    clientMutationId
    payload {
      description
      id
      metadata
      name
    }
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "CreateDepartmentPayload",
    "kind": "LinkedField",
    "name": "createDepartment",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "clientMutationId",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Department",
        "kind": "LinkedField",
        "name": "payload",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "description",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "metadata",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "name",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "CreateDepartmentMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "CreateDepartmentMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "44e56a27f052446bd67b4a1a97184cca",
    "id": null,
    "metadata": {},
    "name": "CreateDepartmentMutation",
    "operationKind": "mutation",
    "text": "mutation CreateDepartmentMutation(\n  $input: CreateDepartmentInput\n) {\n  createDepartment(input: $input) {\n    clientMutationId\n    payload {\n      description\n      id\n      metadata\n      name\n    }\n  }\n}\n"
  }
};
})();
(node as any).hash = '9fd250a4b035d078d4a0cad6331a4395';
export default node;
