/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest } from "relay-runtime";
export type DeleteDepartmentInput = {
    clientMutationId?: string | null;
    id?: string | null;
};
export type DeleteDepartmentMutationVariables = {
    input?: DeleteDepartmentInput | null;
};
export type DeleteDepartmentMutationResponse = {
    readonly deleteDepartment: {
        readonly clientMutationId: string;
    };
};
export type DeleteDepartmentMutation = {
    readonly response: DeleteDepartmentMutationResponse;
    readonly variables: DeleteDepartmentMutationVariables;
};



/*
mutation DeleteDepartmentMutation(
  $input: DeleteDepartmentInput
) {
  deleteDepartment(input: $input) {
    clientMutationId
  }
}
*/

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "DeleteDepartmentPayload",
    "kind": "LinkedField",
    "name": "deleteDepartment",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "clientMutationId",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "DeleteDepartmentMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "DeleteDepartmentMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "1cf4a2410f11c78e3a0be48c700c28dd",
    "id": null,
    "metadata": {},
    "name": "DeleteDepartmentMutation",
    "operationKind": "mutation",
    "text": "mutation DeleteDepartmentMutation(\n  $input: DeleteDepartmentInput\n) {\n  deleteDepartment(input: $input) {\n    clientMutationId\n  }\n}\n"
  }
};
})();
(node as any).hash = 'e35b29af36175afa0c2d88fce18eee53';
export default node;
