import React from 'react'
import { createFragmentContainer } from 'react-relay'
import { graphql } from 'relay-runtime'
import {
    IconButton,
    List,
    ListItem,
    ListItemAvatar,
    ListItemSecondaryAction,
    ListItemText,
    Tooltip,
} from '@material-ui/core'
import { Avatar } from '@saastack/components'
import { DepartmentList_departments } from '../__generated__/DepartmentList_departments.graphql'
import { DeleteOutlined } from '@material-ui/icons'
import { Trans } from '@lingui/macro'
import { makeStyles } from '@material-ui/styles'

interface Props {
    departments: DepartmentList_departments
    onClick: (id: string, action: 'UPDATE' | 'DELETE') => void
}

const useStyles = makeStyles({
    list: {
        '& .MuiListItemSecondaryAction-root': {
            visibility: 'hidden',
        },
        '& li:hover': {
            '& .MuiListItemSecondaryAction-root': {
                visibility: 'visible',
            },
        },
    },
})

const DepartmentList: React.FC<Props> = ({ departments, onClick }) => {
    const classes = useStyles()

    return (
        <List className={classes.list}>
            {departments.map((d) => (
                <ListItem key={d.id} divider onClick={() => onClick(d.id, 'UPDATE')} button>
                    <ListItemAvatar>
                        <Avatar title={d.name} />
                    </ListItemAvatar>
                    <ListItemText primary={d.name} secondary={d.description} />
                    <ListItemSecondaryAction>
                        <Tooltip title={<Trans>Delete {d.name}</Trans>}>
                            <IconButton onClick={() => onClick(d.id, 'DELETE')}>
                                <DeleteOutlined />
                            </IconButton>
                        </Tooltip>
                    </ListItemSecondaryAction>
                </ListItem>
            ))}
        </List>
    )
}

export default createFragmentContainer(DepartmentList, {
    departments: graphql`
        fragment DepartmentList_departments on Department @relay(plural: true) {
            id
            name
            description
        }
    `,
})
