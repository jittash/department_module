import React,{ useState } from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import { DepartmentMaster_departments } from '../__generated__/DepartmentMaster_departments.graphql'
import { Layout } from '@saastack/layouts'
import { Trans } from '@lingui/macro'
import DepartmentList from './DepartmentList'
import { ActionItem } from '@saastack/components/Actions'
import { AddOutlined } from '@material-ui/icons'
import DepartmentAdd from './DepartmentAdd'
import DepartmentUpdate from './DepartmentUpdate'
import DepartmentDelete from './DepartmentDelete'

interface Props {
    departments: DepartmentMaster_departments
    parent: string
    refetch: () => void
}

const DepartmentMaster: React.FC<Props> = ({
    departments: {
        departments: { department: departments },
    },
    parent,
    refetch,
}) => {
    const [openAdd, setOpenAdd] = useState<boolean>(false);
    const [openUpdate, setOpenUpdate] = useState<boolean>(false);
    const [openDelete, setOpenDelete] = useState<boolean>(false);
    const [selected, setSelected] =useState('');
    const variables = {parent};

    const handleAddSuccess = ():void => {
        setOpenAdd(false)
        refetch()
    }
    const handleUpdateSuccess = ():void => {
        setOpenUpdate(false)
        refetch()
    }
    const handleDeleteSuccess = ():void => {
        setOpenDelete(false)
        refetch()
    }
    const handleUpdateClose = ():void => {
        setOpenUpdate(false)
        setSelected('')
    }
    const handleDeleteClose = ():void => {
        setOpenDelete(false)
        setSelected('')
    }
    const handleClick = (id: string, action: 'UPDATE' | 'DELETE'):void => {
        setSelected(id)
        if (action === 'UPDATE') {
            setOpenUpdate(true)
        } else {
            setOpenDelete(true)
        }
    }

    const actions: ActionItem[] = [
        {
            icon: AddOutlined,
            title: <Trans>Add</Trans>,
            onClick: () => setOpenAdd(true),
        },
    ]
   const list = <DepartmentList departments={departments} onClick={handleClick} />

    return (
        <Layout header={<Trans>Departments</Trans>} col1={list} actions={actions} >
            {openAdd && (
                <DepartmentAdd
                    variables={variables}
                    onClose={() => setOpenAdd(false)}
                    onSuccess={handleAddSuccess}
                />
            )}
            {openUpdate && (
                <DepartmentUpdate
                    id={selected}
                    onClose={handleUpdateClose}
                    onSuccess={handleUpdateSuccess}
                    departments={departments}
                />
            )}
            {openDelete && (
                <DepartmentDelete
                    variables={variables}
                    id={selected}
                    onClose={handleDeleteClose}
                    onSuccess={handleDeleteSuccess}
                />
            )}
        </Layout>
    )
}

export default createFragmentContainer(DepartmentMaster,{
    departments: graphql`
        fragment DepartmentMaster_departments on Query
        @argumentDefinitions(parent: { type: "String" }) {
            departments(parent: $parent) {
                department {
                    id
                    name
                    ...DepartmentList_departments
                    ...DepartmentUpdate_departments
                }
            }
        }
    `,
})
