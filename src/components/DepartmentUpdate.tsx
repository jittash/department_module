import { Trans } from '@lingui/macro'
import { Mutable, useAlert } from '@saastack/core'
import { FormContainer } from '@saastack/layouts/containers'
import { FormContainerProps } from '@saastack/layouts/types'
import React, { useEffect } from 'react'
import { createFragmentContainer } from 'react-relay'
import { useRelayEnvironment } from 'react-relay/hooks'
import { graphql } from 'relay-runtime'
import { DepartmentUpdate_departments } from '../__generated__/DepartmentUpdate_departments.graphql'
import { DepartmentInput } from '../__generated__/UpdateDepartmentMutation.graphql'
import DepartmentUpdateFormComponent from '../forms/DepartmentUpdateFormComponent'
import UpdateDepartmentMutation from '../mutations/UpdateDepartmentMutation'
import DepartmentAddValidations from '../utils/DepartmentAddValidations'

interface Props extends Omit<FormContainerProps, 'formId'> {
    id: string
    departments: DepartmentUpdate_departments
    onClose: () => void
    onSuccess: () => void
}

const formId = 'department-update-form'

const DepartmentUpdate: React.FC<Props> = ({ departments, id, onClose, onSuccess, ...props }) => {
    const environment = useRelayEnvironment()
    const showAlert = useAlert()
    const [loading, setLoading] = React.useState<boolean>(false)

    const department = departments.find((i) => i.id === id)!

    useEffect(() => {
        if (!department) {
            onClose()
        }
    }, [department])
    if (!department) {
        return null
    }

    const handleSubmit = (department: DepartmentInput) => {
        setLoading(true)
        UpdateDepartmentMutation.commit(environment, department, ['name', 'description'], {
            onSuccess: handleSuccess,
            onError,
        })
    }
    const onError = (e: string) => {
        setLoading(false)
        showAlert(e, {
            variant: 'error',
        })
    }
    const handleSuccess = (response: DepartmentInput) => {
        setLoading(false)
        showAlert(<Trans>Department updated successfully!</Trans>, {
            variant: 'info',
        })
        onSuccess()
    }

    const initialValues = department as Mutable<DepartmentUpdate_departments[0]>

    return (
        <FormContainer
            open
            onClose={onClose}
            header={<Trans>Update Department</Trans>}
            formId={formId}
            loading={loading}
            {...props}
        >
            <DepartmentUpdateFormComponent
                update
                onSubmit={handleSubmit}
                id={formId}
                initialValues={initialValues}
                validationSchema={DepartmentAddValidations}
            />
        </FormContainer>
    )
}

export default createFragmentContainer(DepartmentUpdate, {
    departments: graphql`
        fragment DepartmentUpdate_departments on Department @relay(plural: true) {
            id
            name
            description
        }
    `,
})
