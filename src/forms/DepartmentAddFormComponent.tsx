import { Trans } from '@lingui/macro'
import { Toggle } from '@saastack/components'
import { Form, Input, Textarea } from '@saastack/forms'
import { FormProps } from '@saastack/forms/types'
import React from 'react'
import { DepartmentInput } from '../__generated__/CreateDepartmentMutation.graphql'

interface Props extends FormProps<DepartmentInput> {
    update?: boolean
}

const DepartmentAddFormComponent: React.FC<Props> = ({ update, ...props }) => {
    const [show, setShow] = React.useState(false)
    return (
        <Form {...props}>
            <Input large name="name" label={<Trans>Title</Trans>} grid={{ xs: 12 }} />
            <Toggle
                label={update ? <Trans>Update description</Trans> : <Trans>Add description</Trans>}
                show={show}
                onShow={setShow}
            >
                <Textarea grid={{ xs: 12 }} label={<Trans>Description</Trans>} name="description" />
            </Toggle>
        </Form>
    )
}

export default DepartmentAddFormComponent
