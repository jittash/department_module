import { MutationCallbacks, setNodeValue } from '@saastack/relay'
import { commitMutation, graphql, Variables } from 'react-relay'
import { Disposable, Environment, RecordProxy, RecordSourceSelectorProxy } from 'relay-runtime'
import {
    CreateDepartmentInput,
    CreateDepartmentMutation,
    CreateDepartmentMutationResponse,
    DepartmentInput,
} from '../__generated__/CreateDepartmentMutation.graphql'

const mutation = graphql`
    mutation CreateDepartmentMutation($input: CreateDepartmentInput) {
        createDepartment(input: $input) {
            clientMutationId
            payload {
               description
               id
               metadata
               name
            }
        }
    }
`

let tempID = 0

const commit = (
    environment: Environment,
    variables: Variables,
    department: DepartmentInput,
    callbacks?: MutationCallbacks<DepartmentInput>
): Disposable => {
    const input: CreateDepartmentInput = {
        parent: variables.parent,
        department,
        clientMutationId: `${tempID++}`,
    }

    return commitMutation<CreateDepartmentMutation>(environment, {
        mutation,
        variables: {
            input,
        },
        onError: (error: Error) => {
            if (callbacks && callbacks.onError) {
                const message = error.message.split('\n')[1]
                callbacks.onError!(message)
            }
        },
        onCompleted: (response: CreateDepartmentMutationResponse) => {
            if (callbacks && callbacks.onSuccess) {
                callbacks.onSuccess({ ...department, ...response.createDepartment.payload })
            }
        },
    })
}

export default { commit }
