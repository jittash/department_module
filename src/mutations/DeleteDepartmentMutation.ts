import { MutationCallbacks } from '@saastack/relay'
import { commitMutation, graphql } from 'react-relay'

import { Disposable, Environment, Variables } from 'relay-runtime'
import {
    DeleteDepartmentInput,
    DeleteDepartmentMutation,
} from '../__generated__/DeleteDepartmentMutation.graphql'

const mutation = graphql`
    mutation DeleteDepartmentMutation($input: DeleteDepartmentInput) {
        deleteDepartment(input: $input) {
            clientMutationId
        }
    }
`

let tempID = 0

const commit = (
    environment: Environment,
    variables: Variables,
    id: string,
    callbacks?: MutationCallbacks<string>
): Disposable => {
    const input: DeleteDepartmentInput = {
        id,
        clientMutationId: `${tempID++}`,
    }

    return commitMutation<DeleteDepartmentMutation>(environment, {
        mutation,
        variables: {
            input,
        },
        onError: (error: Error) => {
            if (callbacks && callbacks.onError) {
                const message = error.message.split('\n')[1]
                callbacks.onError!(message)
            }
        },
        onCompleted: () => {
            if (callbacks && callbacks.onSuccess) {
                callbacks.onSuccess(id)
            }
        },
    })
}

export default { commit }
