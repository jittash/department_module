import React,{ useState,useEffect,useRef } from 'react'
import { graphql } from 'react-relay'
import { useConfig } from '@saastack/core'
import { ErrorComponent, Loading } from '@saastack/components'
import { fetchQuery } from '@saastack/relay'
import { useRelayEnvironment } from 'react-relay/hooks'
import {
    DepartmentPageQuery,
    DepartmentPageQueryResponse,
    DepartmentPageQueryVariables,
} from '../__generated__/DepartmentPageQuery.graphql'
import DepartmentMaster from '../components/DepartmentMaster'

interface Props {}

const query = graphql`
    query DepartmentPageQuery($parent: String) {
        ...DepartmentMaster_departments @arguments(parent: $parent)    
    }
`

const DepartmentPage: React.FC<Props> = ({ ...props }) => {
    const { companyId } = useConfig()
    const [loading, setLoading] = useState<boolean>(true)
    const environment = useRelayEnvironment()
    const [error, setError] = useState<any>(null)
    const dataRef = useRef<DepartmentPageQueryResponse | null>(null)

    const fetchDepartments = () => {
        const variables: DepartmentPageQueryVariables = {
            parent: companyId,
        }
        fetchQuery<DepartmentPageQuery>(environment, query, variables, { force: true })
            .then((resp) => {
                dataRef.current = resp
                setLoading(false)
            })
            .catch((error) => {
                setError(error)
                setLoading(false)
            })
    }
    useEffect(fetchDepartments, [])
    if (loading) {
        return <Loading />
    }
    if (error) {
        return <ErrorComponent error={error} />
    }

    return (
        <DepartmentMaster
            parent={companyId!}
            departments={dataRef.current!}
            refetch={fetchDepartments}
        />
    )
}

export default DepartmentPage
