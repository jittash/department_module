import React from 'react'
import Wrapper from './Wrapper'
import DepartmentPage from '../pages/DepartmentPage'

export default {
    title: 'RelayPractice',
    decorators: [(storyFn: () => JSX.Element) => <Wrapper>{storyFn()}</Wrapper>],
}

export const Default = () => <DepartmentPage />
