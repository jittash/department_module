import { DepartmentInput } from '../__generated__/CreateDepartmentMutation.graphql'

const DepartmentAddInitialValues: DepartmentInput = {
    id: '',
    name: '',
    description: '',
}

export default DepartmentAddInitialValues
